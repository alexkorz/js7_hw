

// TASK 1

let checkRegExp = /[^A-Za-z0-9]/g;
let userInput = prompt('Please enter a string you want to check for palindrome').toLowerCase().replace(checkRegExp, '');

let length = userInput.length;

function isPalindrome(string) {
  for(let i = 0; i < length/2; i++) {
    if(userInput[i] !== userInput[length - 1 - i]) {
      return false;
    }
  }
  return true;
}

isPalindrome() ? console.log('This is a palindrome') : console.log('This is not a palindrome');

// TASK 2

function checkStringLength (string, length) {
  return string.length <= length;
}

console.log(checkStringLength('Test string', 10));

// TASK 3

function calculateAge (birthDate, currentDate) {
  if(birthDate.getFullYear() > currentDate.getFullYear()) {
    console.log('Wrong birth date')
    return;
  }

  return currentDate.getFullYear() - birthDate.getFullYear();
}

let userAge = prompt('Please write your birth date in the format: yyyy-MM-dd');
let currentDate = new Date();

console.log(calculateAge(new Date(userAge), currentDate));